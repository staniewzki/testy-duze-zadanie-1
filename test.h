#include "example.h"
#include "cannot_change_arguments.h"
#include "stress.h"

#include <stdio.h>

#define RUN_TEST(f)				\
	fprintf(stderr, #f "\n");	\
	f()

void Testuj() {
	RUN_TEST(Example);
	RUN_TEST(CannotChangeArguments);
	RUN_TEST(StressCorrect);
	RUN_TEST(StressPerformance);
}
